{ pkgs, buildUtils, ... }:
let inherit (pkgs) callPackage;
in
{
  unite-shell = callPackage ./unite-shell.nix { inherit pkgs; };
  librewolf = callPackage ./librewolf.nix {
    inherit (pkgs) lib;
    inherit (buildUtils) appimagePackage;
  };
  discord-qt = callPackage ./discord-qt.nix {
    inherit (pkgs) lib;
    inherit (buildUtils) appimagePackage;
  };
}
