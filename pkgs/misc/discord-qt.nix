{ lib, appimagePackage }:
appimagePackage rec {
  binName = "discord-qt";
  version = "0.5.0";
  url =
    "https://github.com/ruslang02/discord-qt/releases/download/v${version}/DiscordQt-v${version}-linux-x64.AppImage";
  sha256 = "sha256-1C3LrIGMexwCxboEhqrF16vj8De701y6a2rVYaXDDf0=";
  meta = with lib; {
    platforms = [ "x86_64-linux" ];
    license = licenses.gpl3;
    description = ''
      Discord client powered by Node.JS and Qt.
    '';
  };
}
